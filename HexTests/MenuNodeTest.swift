//
//  MenuNodeTest.swift
//  Hex
//
//  Created by Joshua Stewart on 6/3/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

import XCTest

class MenuNodeTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testOpening() {
        let size = CGSizeMake(640, 480)
        let menu = ESMenuNode()

        // the menu must be in a view controller so update ticks get run
        let vc = UIViewController()
        let view = SKView()
        let scene = SKScene(size: size)
        vc.view = view
        view.presentScene(scene)
        scene.addChild(menu)

        menu.position = CGPointMake(size.width / 2, size.height / 2)


        let expectedInitialRect = CGRectMake(size.width/2, size.height/2, 0, 0)
        XCTAssert(CGRectEqualToRect(menu.frame, expectedInitialRect), "Expected \(NSStringFromCGRect(menu.frame)) to be equal to \(NSStringFromCGRect(expectedInitialRect))")

        menu.size = CGSizeMake(size.width, size.height/2)

        var menuOpenExpectation = self.expectationWithDescription("menu open")
        menu.openWithCompletion({ () -> Void in
            let expectedFinalRect = CGRectMake(0, size.height/4, size.width, size.height/2)
            XCTAssert(CGRectEqualToRect(menu.frame, expectedFinalRect), "Expected menu frame \(NSStringFromCGRect(menu.frame)) to be equal to \(NSStringFromCGRect(expectedFinalRect))")
            menuOpenExpectation.fulfill()
        })
        self.waitForExpectationsWithTimeout(4, nil)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }

}
