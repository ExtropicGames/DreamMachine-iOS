
#import "ESMenuNode.h"

typedef NS_ENUM(NSInteger, ESChoiceMenuRows) {
	ESChoiceMenuRowsSetAutomatically = -1,
};

@class ESChoiceMenuNode;

@protocol ESChoiceMenuNodeDelegate <NSObject>

- (void) choiceMenu:(ESChoiceMenuNode*)choiceMenu didCompleteWithChoice:(NSUInteger)choice;

@end

@interface ESChoiceMenuNode : ESMenuNode

@property (nonatomic, weak) id <ESChoiceMenuNodeDelegate> delegate;

// Array of NSStrings
@property (strong, nonatomic) NSArray* choices;
@property (nonatomic) NSInteger rows;
@property (nonatomic) NSInteger columns;

@end
