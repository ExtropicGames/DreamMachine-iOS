
#import "ESViewController.h"

#import "ESMainMenuScene.h"

@implementation ESViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Configure the view.
    SKView * skView = (SKView *)self.view;

	// Debug options

    skView.showsFPS = YES;
	skView.showsDrawCount = YES;
    skView.showsNodeCount = YES;
	//skView.showsPhysics = YES;
    
    // Create and configure the scene.

	SKScene* scene = [ESMainMenuScene sceneWithSize:skView.bounds.size];
    scene.scaleMode = SKSceneScaleModeResizeFill;
    
    // Present the scene.
    [skView presentScene:scene];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
		return UIInterfaceOrientationMaskPortrait;
    } else {
		// TODO: figure out if/how i'm supporting ipad
        return UIInterfaceOrientationMaskLandscape;
    }
}

@end
