
#import "ESMenuNode.h"

@class ESProfessionMenuNode;

@protocol ESProfessionMenuNodeDelegate <NSObject>

- (void) professionMenuDidComplete:(NSInteger)professionID;

@end

@interface ESProfessionMenuNode : ESMenuNode

- (void) handleTouch:(UITouch*) touch;

@property (nonatomic, weak) id <ESProfessionMenuNodeDelegate> delegate;

@end
