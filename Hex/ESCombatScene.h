//
//  ESCombatScene.h
//  Hex
//
//  Created by Joshua Stewart on 11/14/13.
//  Copyright (c) 2013 Extropic Studios. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

#import "ESChoiceMenuNode.h"

@interface ESCombatScene : SKScene <ESChoiceMenuNodeDelegate>

@end
