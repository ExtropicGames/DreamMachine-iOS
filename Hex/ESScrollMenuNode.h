//
//  ESScrollMenu.h
//  Hex
//
//  Created by Joshua Stewart on 11/24/13.
//  Copyright (c) 2013 Extropic Studios. All rights reserved.
//

#import "ESMenuNode.h"

@interface ESScrollMenuNode : ESMenuNode

// Adds a line to the bottom, causing it to scroll up.
- (void) addLine:(NSString*) line, ...;

@end
