//
//  SKSpriteNode+Extropic.h
//  Hex
//
//  Created by Joshua Stewart on 11/23/13.
//  Copyright (c) 2013 Extropic Studios. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface SKSpriteNode (Extropic)

-(void) setBottomLeftCornerPosition:(CGPoint) newPosition;

@end
