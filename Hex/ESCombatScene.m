
#import "ESScrollMenuNode.h"
#import "ESChoiceMenuNode.h"

#import "ESCombatScene.h"

#import "Character.h"

// The size of the bottom panel where the menus appear.
const int MENU_HEIGHT = 164;

// got a lil baby state machine inside this guy
typedef NS_ENUM(NSUInteger, ESCombatSceneState) {
	STATE_CHOOSE_ACTIONS,
	STATE_DO_ROUND
};

@implementation ESCombatScene
{
	ESAppDelegate* _app;
	ESChoiceMenuNode* _leftMenu;
	ESChoiceMenuNode* _rightMenu;
	int _currentCharacterIdx;
	Character* _currentCharacter;
	ESCombatSceneState _currentState;
}

#pragma mark - SKScene

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
		self.scaleMode = SKSceneScaleModeResizeFill;
		_app = [UIApplication sharedApplication].delegate;

        self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.3 alpha:1.0];

		// Add monster
		SKSpriteNode* monster = [SKSpriteNode spriteNodeWithImageNamed:@"monster"];
		[monster setBottomLeftCornerPosition:CGPointMake(32, 256)];
		[self addChild:monster];

		// Add character sprites.
		// TODO: make this based off the actual party members
		__block CGFloat offset = 48;
		[_app.party.characters enumerateObjectsUsingBlock:^(Character* character, NSUInteger idx, BOOL *stop) {
			NSArray* professionImageMap = @[@"hunter",
											@"clockwork",
											@"ghost",
											@"priest"];
			SKSpriteNode* player = [SKSpriteNode spriteNodeWithImageNamed:professionImageMap[character.profession]];
			[player setBottomLeftCornerPosition:CGPointMake(offset, 164)];
			[self addChild:player];
			offset += 64;
		}];

        _leftMenu = [[ESChoiceMenuNode alloc] init];
        // TODO: set up the left menu correctly
        _leftMenu.position = CGPointMake(0, 0);
		_leftMenu.columns = 1;
        _leftMenu.delegate = self;
        [self addChild:_leftMenu];

        _rightMenu = [[ESChoiceMenuNode alloc] init];
        // TODO: set up the right menu correctly
        _rightMenu.position = CGPointMake(0, 0);
		_rightMenu.columns = 1;
        _rightMenu.delegate = self;
        [self addChild:_rightMenu];

		_currentCharacterIdx = 0;
		[self updateMenu];

    }
    return self;
}

-(void)update:(NSTimeInterval)currentTime {
	if (_currentState != STATE_DO_ROUND) {
		return;
	}

	// TODO: show the events... AS THEY HAPPEN.
}

#pragma mark - ESChoiceMenuNodeDelegate

-(void)choiceMenu:(ESChoiceMenuNode *)choiceMenu didCompleteWithChoice:(NSUInteger)choice {

    if (choiceMenu == _leftMenu) {

        if (choice == 0) { // Ability
            // TODO: optimize this block
            NSMutableArray* abilities = [NSMutableArray array];
            for (Ability* a in _currentCharacter.abilities) {
                [abilities addObject:a.name];
            }
            _rightMenu.choices = abilities;
        } else if (choice == 1) { // Hormone
            NSMutableArray* spells = [NSMutableArray array];
            for (Ability* a in _currentCharacter.spells) {
                [spells addObject:a.name];
            }
            _rightMenu.choices = spells;
        } else if (choice == 2) { // Item
            NSMutableArray* items = [NSMutableArray array];
            for (Item* i in _currentCharacter.inventory) {
                [items addObject:i.name];
            }
            _rightMenu.choices = items;
        } else if (choice == 3) { // Run
            if (_currentCharacterIdx == 0) {
                ESCombatScene* combatScene = [[ESCombatScene alloc] initWithSize:self.size];
                combatScene.scaleMode = SKSceneScaleModeResizeFill;
                [self.scene.view presentScene:combatScene transition:nil];
                // TODO: run
            } else {
                _currentCharacterIdx--;
                [self updateMenu];
            }
        }
        NSLog(@"you chose %@", _leftMenu.choices[choice]);

    } else if (choiceMenu == _rightMenu) {
        NSLog(@"you chose %@", _rightMenu.choices[choice]);
        _currentCharacterIdx++;
        if (_currentCharacterIdx >= _app.party.characters.count) {
            // all characters have chosen their actions, now do the round
            _currentState = STATE_DO_ROUND;
            _leftMenu.hidden = YES;
            _rightMenu.hidden = YES;
            _currentCharacterIdx = 0;
        }
        [self updateMenu];
    }
}


#pragma mark - Private Methods

-(void)updateMenu {
	NSAssert(_currentCharacterIdx < _app.party.characters.count, @"Current character index was out of bounds!");
	_currentCharacter = _app.party.characters[_currentCharacterIdx];
	NSArray* spellsNameMap = @[@"Hormone",
							   @"Gear",
							   @"Shell",
							   @"▲"];
	_leftMenu.choices = @[@"Ability", spellsNameMap[_currentCharacter.profession], @"Item", _currentCharacterIdx == 0 ? @"Run" : @"Back" ];
	_rightMenu.choices = @[];
}

-(void)setMenuOptions {
	// TODO
}

@end
