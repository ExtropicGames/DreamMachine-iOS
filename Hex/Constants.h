//
//  Constants.h
//  Hex
//
//  Created by Joshua Stewart on 11/17/13.
//  Copyright (c) 2013 Extropic Studios. All rights reserved.
//

extern const int TILE_SIZE;
extern const int FONT_SIZE;
extern const int PADDING;