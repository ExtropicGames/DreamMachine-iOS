
#import <Foundation/Foundation.h>

typedef struct NSIntegerPoint {
	NSInteger x;
	NSInteger y;
} NSIntegerPoint;

// I forget where these come from or what they do.

CG_INLINE NSIntegerPoint NSIntegerPointMake(NSInteger x, NSInteger y) {
	NSIntegerPoint p; p.x = x; p.y = y; return p;
}

CG_INLINE bool __NSIntegerPointEqualToPoint(NSIntegerPoint point1, NSIntegerPoint point2) {
	return point1.x == point2.x && point1.y == point2.y;
}

#define NSIntegerPointEqualToPoint __NSIntegerPointEqualToPoint

// Log statements that compile out in release builds.

#ifdef DEBUG
#define L(fmt, ...) fprintf(stderr,"%s [Line %d] %s\n", __FUNCTION__, __LINE__, [[NSString stringWithFormat:fmt, ##__VA_ARGS__] UTF8String])
#define LS(fmt, ...) fprintf(stderr,"%s\n", [[NSString stringWithFormat:fmt, ##__VA_ARGS__] UTF8String])
#else
#define L(...)
#define LS(...)
#endif