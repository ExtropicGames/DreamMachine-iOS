//
//  Ability+Methods.h
//  Hex
//
//  Created by Joshua Stewart on 1/18/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

#import "Ability.h"

@interface Ability (Methods)

+ (Ability*) abilityWithName:(NSString*) name context:(NSManagedObjectContext*) context;

@end
