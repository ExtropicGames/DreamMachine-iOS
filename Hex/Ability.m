//
//  Ability.m
//  Hex
//
//  Created by Joshua Stewart on 1/18/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

#import "Ability.h"
#import "Character.h"


@implementation Ability

@dynamic name;
@dynamic character_abilities;
@dynamic character_spells;

@end
