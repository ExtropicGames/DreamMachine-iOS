//
//  Constants.m
//  Hex
//
//  Created by Joshua Stewart on 11/17/13.
//  Copyright (c) 2013 Extropic Studios. All rights reserved.
//

#import "Constants.h"

// Size of a tile is 32x32. Scaled at 2x, that means tiles are 16x16 pixels. (Retina tiles are 64x64).
// Size of a tile is 16x16 pixels.
// Size of a character is 16x32 pixels.
// Text characters are 8x8 pixels.
// The region in which enemies can appear is 128x96 pixels.
// Huge (full screen) enemies are 128x96 pixels.
// Tall (1/2 screen) enemies are 60x96 pixels.
// Wide (1/2 screen) enemies are 128x44 pixels.
// Need to figure out screen resolutions for 1/4 screen and 1/8 screen enemies.

// All constants are doubled because sprites are scaled up 2x.

const int TILE_SIZE = 32;

const int FONT_SIZE = 16;

// For menu padding.
const int PADDING = 16;