//
//  ESAppDelegate.h
//  Hex
//
//  Created by Joshua Stewart on 11/13/13.
//  Copyright (c) 2013 Extropic Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Party.h"

@interface ESAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) Party *party;

#pragma mark - Core Data Properties & Methods

@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (void)saveContext;
- (NSURL*)applicationDocumentsDirectory;

@end
