//
//  ESMainMenuScene.h
//  Hex
//
//  Created by Joshua Stewart on 3/22/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

#import "ESChoiceMenuNode.h"
#import "ESProfessionMenuNode.h"
#import "Hex-Swift.h"

@interface ESMainMenuScene : SKScene <ESChoiceMenuNodeDelegate, ESProfessionMenuNodeDelegate, NameMenuNodeDelegate>

@end
