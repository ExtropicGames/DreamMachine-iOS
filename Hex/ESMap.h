//
//  ESMap.h
//  Hex
//
//  Created by Joshua Stewart on 11/14/13.
//  Copyright (c) 2013 Extropic Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESMap : NSObject <NSCoding>

#pragma mark - Methods

- (id)initWithJSONFile:(NSString *)filename;

#pragma mark - Properties

// Size of each edge of the map. Maps are square for now for simplicity.
@property NSInteger size;
// Array of NSStrings. Contain the texture names used by the map.
@property (strong, nonatomic) NSArray* textures;
// Array of NSIntegers. Contain indexes to the textures array. Length is _size^2.
@property (strong, nonatomic) NSArray* tileTextures;
// Array of NSIntegers. Represent properties of the tile. Length is _size^2.
@property (strong, nonatomic) NSArray* tileProperties;

@end
