//
//  NSManagedObjectContext+Extropic.h
//  Hex
//
//  Created by Joshua Stewart on 1/11/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (Extropic)

- (NSSet*)fetchObjectsForEntityName:(NSString*)newEntityName withPredicate:(id)stringOrPredicate, ...;

@end
