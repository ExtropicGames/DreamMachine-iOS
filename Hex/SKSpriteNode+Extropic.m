//
//  SKSpriteNode+Extropic.m
//  Hex
//
//  Created by Joshua Stewart on 11/23/13.
//  Copyright (c) 2013 Extropic Studios. All rights reserved.
//

#import "SKSpriteNode+Extropic.h"

@implementation SKSpriteNode (Extropic)

-(void) setBottomLeftCornerPosition:(CGPoint) newPosition {
	self.position = CGPointMake(newPosition.x + (self.size.width / 2), newPosition.y + (self.size.height / 2));
}

@end
