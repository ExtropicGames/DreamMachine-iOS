//
//  ESScrollMenu.m
//  Hex
//
//  Created by Joshua Stewart on 11/24/13.
//  Copyright (c) 2013 Extropic Studios. All rights reserved.
//

#import "ESScrollMenuNode.h"

@implementation ESScrollMenuNode
{
	SKCropNode* _scrollBounds;
	SKNode* _scrollNode;

}

#pragma mark - SKNode

- (instancetype) init {
    self = [super init];
    if (self) {
        // TODO: figure out if i broke this
        _scrollBounds = [SKCropNode node];
        //_scrollBounds.position = center;
        _scrollBounds.maskNode = [SKSpriteNode spriteNodeWithImageNamed:@"black"];
        //_scrollBounds.maskNode.xScale = size.width - PADDING;
        //_scrollBounds.maskNode.yScale = size.height - PADDING;

        [self addChild:_scrollBounds];

        _scrollNode = [SKNode node];
        [_scrollBounds addChild:_scrollNode];
    }

    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        // TODO: figure out if i broke this
        _scrollBounds = [SKCropNode node];
        //_scrollBounds.position = center;
        _scrollBounds.maskNode = [SKSpriteNode spriteNodeWithImageNamed:@"black"];
        //_scrollBounds.maskNode.xScale = size.width - PADDING;
        //_scrollBounds.maskNode.yScale = size.height - PADDING;

        [self addChild:_scrollBounds];

        _scrollNode = [SKNode node];
        [_scrollBounds addChild:_scrollNode];
    }

    return self;
}

#pragma mark Getters & Setters

- (void) setHidden:(BOOL)hidden {
	[super setHidden:hidden];
	_scrollBounds.hidden = hidden;
}

- (void) setTextAlign:(ESMenuTextAlign)textAlign {
	[super setTextAlign:textAlign];

	// TODO: make this class respect text alignment
}

#pragma mark - Public Methods

- (void) addLine:(NSString *)line, ... {
	SKLabelNode* previous = _scrollNode.children.lastObject;

	SKLabelNode* label = [SKLabelNode labelNodeWithFontNamed:@"Terminus"];
	label.fontSize = FONT_SIZE;
	label.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
	if (previous == nil) {
		label.position = CGPointMake((-self.frame.size.width / 2) + PADDING, (-self.frame.size.height / 2) + PADDING - FONT_SIZE);
	} else {
		label.position = CGPointMake((-self.frame.size.width / 2) + PADDING, previous.position.y - FONT_SIZE);
	}

	va_list args;
	va_start(args, line);
	label.text = [NSString stringWithFormat:line, args];
	va_end(args);

	// TODO: if text is longer than width of menu, automatically wrap it
	// TODO: delete label nodes that are above the top of _scrollBounds

	[_scrollNode addChild:label];
	[_scrollNode runAction:[SKAction moveByX:0 y:FONT_SIZE duration:0.5]];
}

@end