//
//  ESMap.h
//  Hex
//
//  Created by Joshua Stewart on 11/13/13.
//  Copyright (c) 2013 Extropic Studios. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

#import "ESMap.h"

@interface ESMapRenderer : NSObject

#pragma mark - Methods

-(instancetype) init __attribute__((unavailable("use initWithScene instead")));
-(instancetype) initWithScene:(SKScene*) scene;

-(NSIntegerPoint)getTileAtTouchLocation:(CGPoint) location;
-(void)scrollToTile:(NSIntegerPoint) tile;
-(BOOL)isScrolling;

#pragma mark - Properties

@property (strong, nonatomic) ESMap* map;

@end
