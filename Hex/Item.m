//
//  Item.m
//  Hex
//
//  Created by Joshua Stewart on 1/18/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

#import "Item.h"
#import "Character.h"


@implementation Item

@dynamic name;
@dynamic count;
@dynamic owner;

@end
