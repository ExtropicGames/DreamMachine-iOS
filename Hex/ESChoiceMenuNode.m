
#import "ESChoiceMenuNode.h"

@implementation ESChoiceMenuNode
{
	NSMutableArray* _labelNodes;
}

#pragma mark - Initialization

- (instancetype) init {
    if (self = [super init]) {
        [self ESChoiceMenuNode_sharedInit];
    }
    return self;
}
- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self ESChoiceMenuNode_sharedInit];
    }
    return self;
}

- (void) ESChoiceMenuNode_sharedInit {
    _choices = @[];
    _columns = 1;
    _rows = ESChoiceMenuRowsSetAutomatically;
}

#pragma mark - UIResponder

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

	// TODO: this method needs tweaking & optimization

	const CGFloat rowCount = (_rows == ESChoiceMenuRowsSetAutomatically) ? _choices.count : _rows;

    const CGFloat columnWidth = self.size.width / _columns;
	const CGFloat rowHeight = self.size.height / rowCount;

    for (UITouch* touch in touches) {
        for (SKLabelNode* label in _labelNodes) {
            CGPoint location = [touch locationInNode:label];
            if (location.y > -rowHeight / 2 && location.y < rowHeight / 2 && location.x > -(columnWidth / 2) && location.x < (columnWidth/2)) {
                [self.delegate choiceMenu:self didCompleteWithChoice:[_labelNodes indexOfObject:label]];
            }
        }
    }
}

#pragma mark - ESMenuNode

- (void) openWithCompletion:(void (^)())completion {
	[super openWithCompletion:^{
		self.choices = _choices;
		if (completion) {
			completion();
		}
	}];
}

#pragma mark - Getters & Setters

-(void) setChoices:(NSArray *)choices {

	_choices = choices;

	if (_labelNodes) {
		[self.menuBoundsNode removeChildrenInArray:_labelNodes];
	}

	// TODO: make it so we only have to recreate the label nodes and reposition them if the number of choices changes

	_labelNodes = [[NSMutableArray alloc] init];

	for (NSString* choice in choices) {
		SKLabelNode* node = [SKLabelNode labelNodeWithFontNamed:@"Terminus"];
		node.fontSize = FONT_SIZE;
		node.text = choice;
		[self.menuBoundsNode addChild:node];
		[_labelNodes addObject:node];
	}

	[self repositionContents];
}

-(void) setRows:(NSInteger)rows {
	if (rows == _rows) {
		return;
	}
	_rows = rows;
	[self repositionContents];
}

-(void) setColumns:(NSInteger)columns {
	if (columns == _columns) {
		return;
	}
	_columns = columns;
	[self repositionContents];
}

#pragma mark - Private methods

- (void) repositionContents {

	const CGFloat rowCount = (_rows == ESChoiceMenuRowsSetAutomatically) ? _choices.count : _rows;

	const CGFloat top = self.size.height / 2.0;
	const CGFloat rowHeight = self.size.height / rowCount;

    // TODO: col is currently not being used in the calculation
	CGFloat col = 0;
	CGFloat row = 0;

	for (SKLabelNode* label in _labelNodes) {
        const CGFloat y = top - ((row + 0.5) * rowHeight); // the 0.5 is to center it in the row

		if (self.textAlign == ESMenuTextAlignCenter) {
			label.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
		} else if (self.textAlign == ESMenuTextAlignLeft) {
            label.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
		} else if (self.textAlign == ESMenuTextAlignRight) {
            label.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeRight;
		}

        label.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        label.position = CGPointMake(0, y);

		col++;
		if (col >= _columns) {
			col = 0;
			row++;
		}
	}
}

@end
