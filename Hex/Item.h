//
//  Item.h
//  Hex
//
//  Created by Joshua Stewart on 1/18/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Character;

@interface Item : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic) int32_t count;
@property (nonatomic, retain) Character *owner;

@end
