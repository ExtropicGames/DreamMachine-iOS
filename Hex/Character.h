//
//  Character.h
//  Hex
//
//  Created by Joshua Stewart on 1/18/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Ability, Item, Party;

@interface Character : NSManagedObject

@property (nonatomic) int16_t profession;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Party *party;
@property (nonatomic, retain) NSOrderedSet *abilities;
@property (nonatomic, retain) NSOrderedSet *spells;
@property (nonatomic, retain) NSOrderedSet *inventory;
@end

@interface Character (CoreDataGeneratedAccessors)

- (void)insertObject:(Ability *)value inAbilitiesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromAbilitiesAtIndex:(NSUInteger)idx;
- (void)insertAbilities:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeAbilitiesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInAbilitiesAtIndex:(NSUInteger)idx withObject:(Ability *)value;
- (void)replaceAbilitiesAtIndexes:(NSIndexSet *)indexes withAbilities:(NSArray *)values;
- (void)addAbilitiesObject:(Ability *)value;
- (void)removeAbilitiesObject:(Ability *)value;
- (void)addAbilities:(NSOrderedSet *)values;
- (void)removeAbilities:(NSOrderedSet *)values;
- (void)insertObject:(Ability *)value inSpellsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromSpellsAtIndex:(NSUInteger)idx;
- (void)insertSpells:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeSpellsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInSpellsAtIndex:(NSUInteger)idx withObject:(Ability *)value;
- (void)replaceSpellsAtIndexes:(NSIndexSet *)indexes withSpells:(NSArray *)values;
- (void)addSpellsObject:(Ability *)value;
- (void)removeSpellsObject:(Ability *)value;
- (void)addSpells:(NSOrderedSet *)values;
- (void)removeSpells:(NSOrderedSet *)values;
- (void)insertObject:(Item *)value inInventoryAtIndex:(NSUInteger)idx;
- (void)removeObjectFromInventoryAtIndex:(NSUInteger)idx;
- (void)insertInventory:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeInventoryAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInInventoryAtIndex:(NSUInteger)idx withObject:(Item *)value;
- (void)replaceInventoryAtIndexes:(NSIndexSet *)indexes withInventory:(NSArray *)values;
- (void)addInventoryObject:(Item *)value;
- (void)removeInventoryObject:(Item *)value;
- (void)addInventory:(NSOrderedSet *)values;
- (void)removeInventory:(NSOrderedSet *)values;
@end
