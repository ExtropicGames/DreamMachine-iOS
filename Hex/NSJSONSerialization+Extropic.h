
#import <Foundation/Foundation.h>

@interface NSJSONSerialization (Extropic)

+ (id) JSONObjectWithFile:(NSString*)filename;

@end
