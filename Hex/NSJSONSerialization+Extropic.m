
#import "NSJSONSerialization+Extropic.h"

@implementation NSJSONSerialization (Extropic)

+ (id) JSONObjectWithFile:(NSString *)filename {

	NSString* file = [[NSBundle mainBundle] pathForResource:filename ofType:@"json"];
	NSString* json = [NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil];
	NSAssert(json, @"could not read json from %@", file);
	NSError* error;
	id result = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
	if (error) {
		LS(@"Error in file %@.json: %@", filename, error.userInfo[@"NSDebugDescription"]);
	}
	return result;
}

@end
