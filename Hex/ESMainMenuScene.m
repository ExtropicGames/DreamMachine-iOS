
#import "ESWorldScene.h"

#import "ESMainMenuScene.h"

@implementation ESMainMenuScene
{
    ESAppDelegate* app;
    ESMenuNode* currentMenu;
    NameMenuNode* nameMenu;
    ESProfessionMenuNode* professionMenu;
    NSString* name;
}

#pragma mark - SKScene

-(id)initWithSize:(CGSize)size {

	if (self = [super initWithSize:size]) {

		self.scaleMode = SKSceneScaleModeResizeFill;
        app = [UIApplication sharedApplication].delegate;

        // Set up the initial choice menu

        ESChoiceMenuNode* choiceMenu = [[ESChoiceMenuNode alloc] init];
		choiceMenu.position = CGPointMake(size.width / 2, size.height / 2);
		choiceMenu.size = CGSizeMake(size.width, size.height/2);
        choiceMenu.columns = 1;
        choiceMenu.textAlign = ESMenuTextAlignCenter;
		choiceMenu.delegate = self;
        [self addChild:choiceMenu];
		[choiceMenu open];
		currentMenu = choiceMenu;
		choiceMenu.choices = @[@"New Game", @"Load Game", @"Settings"];
	}

	return self;
}

#pragma mark - ESChoiceMenuNodeDelegate

-(void)choiceMenu:(ESChoiceMenuNode *)choiceMenu didCompleteWithChoice:(NSUInteger)choice {
    if (choice == 0) {
		[choiceMenu closeWithCompletion:^{
			[choiceMenu removeFromParent];
			app.party = [NSEntityDescription insertNewObjectForEntityForName:@"Party" inManagedObjectContext:app.managedObjectContext];

			nameMenu = [[NameMenuNode alloc] init];
			nameMenu.position = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
			nameMenu.size = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height/2);
			nameMenu.delegate = self;
			[self addChild:nameMenu];
			[nameMenu open];
			currentMenu = nameMenu;
		}];
    } else if (choice == 1) {
        // TODO: select which game to load, etc

        // TODO: add core data support for savegames

        //ESWorldScene* worldScene = [[ESWorldScene alloc] initWithSize:self.size];
        //[self.scene.view presentScene:worldScene transition:nil];
    }
}

#pragma mark - ESNameMenuNodeDelegate

- (void)nameMenuDidCompleteWithName:(NSString *)newName {
    name = newName;
    [nameMenu removeFromParent];

    professionMenu = [[ESProfessionMenuNode alloc] init];

    // TODO: set up the profession menu

    professionMenu.position = CGPointMake(0, 0);
    professionMenu.delegate = self;
    [self addChild:professionMenu];
}

#pragma mark - ESProfessionMenuNodeDelegate

-(void)professionMenuDidComplete:(NSInteger)professionID {
    [professionMenu removeFromParent];

    Character* c = [Character characterWithName:name profession:professionID context:app.managedObjectContext];

    // TODO: load default abilities for c

    /*

    // Load abilities

    // TODO: Check http://en.wikipedia.org/wiki/Endocrine_system for hormone list

    id abilities = [NSJSONSerialization JSONObjectWithFile:@"Abilities"];

    for (Character* c in app.party.characters) {

        NSMutableArray* abilityArray = [NSMutableArray array];
        NSMutableArray* spellArray = [NSMutableArray array];

        NSString* professionName = [Character getNameForProfession:c.profession];
        for (NSString* abilityName in abilities[professionName]) {
            Ability* ability = [Ability abilityWithName:abilityName context:_app.managedObjectContext];
            if (abilities[professionName][abilityName][@"is_ability"]) {
                LS(@"Loaded ability %@ for class %@", abilityName, professionName);
                [abilityArray addObject:ability];
            } else {
                LS(@"Loaded spell %@ for class %@", abilityName, professionName);
                [spellArray addObject:ability];
            }
        }

        c.abilities = [NSOrderedSet orderedSetWithArray:abilityArray];
        c.spells = [NSOrderedSet orderedSetWithArray:spellArray];
    }

     */

    [app.party addCharactersObject:c];

    [app saveContext];

    if (app.party.characters.count < 4) {
        nameMenu = [[NameMenuNode alloc] init];
        // TODO: set up the name menu
        nameMenu.position = CGPointMake(0, 0);
        nameMenu.delegate = self;
        [self addChild:nameMenu];
    } else {
        ESWorldScene* worldScene = [[ESWorldScene alloc] initWithSize:self.size];
        [self.scene.view presentScene:worldScene transition:nil];
    }
}

@end
