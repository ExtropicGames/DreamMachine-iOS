//
//  ESMap.m
//  Hex
//
//  Created by Joshua Stewart on 11/14/13.
//  Copyright (c) 2013 Extropic Studios. All rights reserved.
//

#import "ESMap.h"

@implementation ESMap
{}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }

	_size = [decoder decodeIntegerForKey:@"size"];
    _textures = [decoder decodeObjectForKey:@"textures"];
    _tileTextures = [decoder decodeObjectForKey:@"tileTextures"];
	_tileProperties = [decoder decodeObjectForKey:@"tileProperties"];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInteger:_size forKey:@"size"];
    [encoder encodeObject:_textures forKey:@"textures"];
    [encoder encodeObject:_tileTextures forKey:@"tileTextures"];
	[encoder encodeObject:_tileProperties forKey:@"tileProperties"];
}

#pragma mark - Public methods

- (id)initWithJSONFile:(NSString *)filename {
	self = [super init];
	if (!self) {
		return nil;
	}

	NSDictionary* result = [NSJSONSerialization JSONObjectWithFile:filename];

	// TODO: more asserts!
	NSAssert(result, @"result is nil");
	NSAssert(result[@"size"], @"result did not contain size");

	_size = [result[@"size"] integerValue];
	_textures = result[@"textures"];
	_tileTextures = result[@"tileTextures"];
	_tileProperties = result[@"tileProperties"];

	return self;
}

@end
