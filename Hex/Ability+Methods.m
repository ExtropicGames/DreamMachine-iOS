//
//  Ability+Methods.m
//  Hex
//
//  Created by Joshua Stewart on 1/18/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

#import "Ability+Methods.h"

@implementation Ability (Methods)

+ (Ability*) abilityWithName:(NSString*) name context:(NSManagedObjectContext*) context {
	Ability* result = [[context fetchObjectsForEntityName:@"Ability" withPredicate:@"name == %@", name] anyObject];
	if (!result) {
		result = [NSEntityDescription insertNewObjectForEntityForName:@"Ability" inManagedObjectContext:context];
		result.name = name;
	}
	return result;
}

@end
