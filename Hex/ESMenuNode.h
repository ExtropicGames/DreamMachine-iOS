
#import <Foundation/Foundation.h>
@import SpriteKit;

typedef NS_ENUM(NSInteger, ESMenuTextAlign) {
	ESMenuTextAlignLeft,
	ESMenuTextAlignCenter,
	ESMenuTextAlignRight
};

@interface ESMenuNode : SKNode

- (void) open;
- (void) openWithCompletion:(void (^)())completion;
- (void) close;
- (void) closeWithCompletion:(void (^)())completion;
- (void) repositionContents;

@property (nonatomic, readonly) BOOL isOpen;
@property (nonatomic) CGSize size;

@property (nonatomic, strong) NSString* cornerImageName;
@property (nonatomic, strong) NSString* borderImageName;
@property (nonatomic, strong) NSString* backgroundImageName;

// TODO: this should be an ivar but making it a property because i can't figure out how to access ivars from swift
@property (nonatomic) SKCropNode* menuBoundsNode;

// TODO: figure out if this should live here or in a child class
@property (nonatomic) ESMenuTextAlign textAlign; // Default is ESMenuTextAlignLeft.

@end
