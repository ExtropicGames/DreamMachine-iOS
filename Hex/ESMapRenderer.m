//
//  ESMap.m
//  Hex
//
//  Created by Joshua Stewart on 11/13/13.
//  Copyright (c) 2013 Extropic Studios. All rights reserved.
//

#import "ESMapRenderer.h"

@implementation ESMapRenderer
{
	SKScene* _scene;
    NSString* _name;
	// TODO: maybe making this into an SKCropNode would improve framerate?
	SKNode* _root;
	// Array of SKTextures
	NSMutableArray* _textures;
}

-(instancetype) initWithScene:(SKScene*) scene {
    self = [super init];
    if (self) {
		_scene = scene;
        _name = [[NSUUID UUID] UUIDString];
		_root = [SKNode node];
		_textures = [[NSMutableArray alloc] init];

		[scene addChild:_root];
    }
    return self;
}

-(NSIntegerPoint)getTileAtTouchLocation:(CGPoint) location {
	NSLog(@"touch at location %f, %f", location.x, location.y);
	NSLog(@"root is offset by %f, %f", _root.position.x, _root.position.y);;
	NSIntegerPoint result = NSIntegerPointMake((location.x - _root.position.x) / TILE_SIZE,
											   (location.y - _root.position.y) / TILE_SIZE);
	NSLog(@"touched tile %ld, %ld", (long)result.x, (long)result.y);
	return result;
}

-(void)scrollToTile:(NSIntegerPoint) tile {
	// i hate spritekit's coordinate system
	CGPoint destination = CGPointMake((-tile.x * TILE_SIZE) + (_scene.size.width / 2),
									  (-tile.y * TILE_SIZE) + (_scene.size.height / 2));
	// TODO: get the distance from the current tile to the new tile and set duration based on that
	[_root runAction:[SKAction moveTo:destination duration:0.5]];
}

-(void) setMap:(ESMap *)map {
	_map = map;

	[_textures removeAllObjects];
	for (NSString* textureName in _map.textures) {
		SKTexture* tex = [SKTexture textureWithImageNamed:textureName];
		tex.filteringMode = SKTextureFilteringNearest;
		[_textures addObject:tex];
	}

	// clear out the old sprites
	[_root removeAllChildren];

	// TODO: right now we are just creating a separate sprite node for every
	// tile in the map. if this turns out to not be performant then we can look
	// at alternate solutions.
	// Can look into SpriteBatchNode and TextureAtlas.
	// We add the sprites in reverse order so that sprites in the background
	// are drawn behind sprites in the foreground.
	for (long r = _map.size - 1; r >= 0; r--) {
		for (long c = _map.size - 1; c >= 0; c--) {
			SKSpriteNode* sprite = [SKSpriteNode node];
			sprite.texture = _textures[[_map.tileTextures[r + (c * _map.size)] integerValue]];
			sprite.size = sprite.texture.size;
			sprite.position = CGPointMake(r * TILE_SIZE, (c * TILE_SIZE) - ((TILE_SIZE - sprite.size.height) / 2));
			sprite.name = _name;
			[_root addChild:sprite];
		}
	}
}

- (BOOL) isScrolling {
	return _root.hasActions;
}

@end
