//
//  Character.m
//  Hex
//
//  Created by Joshua Stewart on 1/18/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

#import "Character.h"
#import "Ability.h"
#import "Item.h"
#import "Party.h"


@implementation Character

@dynamic profession;
@dynamic name;
@dynamic party;
@dynamic abilities;
@dynamic spells;
@dynamic inventory;

@end
