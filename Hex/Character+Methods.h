//
//  Character+Methods.h
//  Hex
//
//  Created by Joshua Stewart on 1/18/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

#import "Character.h"

typedef NS_ENUM(int16_t, Profession) {
	PROFESSION_HUNTER = 0,
	PROFESSION_CLOCKWORK = 1,
	PROFESSION_GHOST = 2,
	PROFESSION_PRIEST = 3
};

@interface Character (Methods)

+ (Character*) characterWithName:(NSString*) name profession:(Profession) profession context:(NSManagedObjectContext*) context;

+ (NSString*) getNameForProfession:(int16_t) profession;

@end
