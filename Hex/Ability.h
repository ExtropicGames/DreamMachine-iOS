//
//  Ability.h
//  Hex
//
//  Created by Joshua Stewart on 1/18/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Character;

@interface Ability : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *character_abilities;
@property (nonatomic, retain) NSSet *character_spells;
@end

@interface Ability (CoreDataGeneratedAccessors)

- (void)addCharacter_abilitiesObject:(Character *)value;
- (void)removeCharacter_abilitiesObject:(Character *)value;
- (void)addCharacter_abilities:(NSSet *)values;
- (void)removeCharacter_abilities:(NSSet *)values;

- (void)addCharacter_spellsObject:(Character *)value;
- (void)removeCharacter_spellsObject:(Character *)value;
- (void)addCharacter_spells:(NSSet *)values;
- (void)removeCharacter_spells:(NSSet *)values;

@end
