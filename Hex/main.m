//
//  main.m
//  Hex
//
//  Created by Joshua Stewart on 11/13/13.
//  Copyright (c) 2013 Extropic Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ESAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([ESAppDelegate class]));
	}
}
