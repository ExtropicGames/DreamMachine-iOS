//
//  Character+Methods.m
//  Hex
//
//  Created by Joshua Stewart on 1/18/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

#import "Character+Methods.h"

@implementation Character (Methods)

+ (Character*) characterWithName:(NSString*) name profession:(Profession) profession context:(NSManagedObjectContext*) context {
	Character* result = [NSEntityDescription insertNewObjectForEntityForName:@"Character" inManagedObjectContext:context];
	result.profession = profession;
	result.name = name;
	return result;
}

+ (int16_t) getIDForProfession:(NSString*)name {
	NSDictionary* professions = @{
		@"hunter" : @(PROFESSION_HUNTER),
		@"clockwork" : @(PROFESSION_CLOCKWORK),
		@"ghost" : @(PROFESSION_GHOST),
		@"priest" : @(PROFESSION_PRIEST)
	};

	return [professions[name] intValue];
}

+ (NSString*) getNameForProfession:(int16_t)profession {
	NSArray* professions = @[@"hunter", @"clockwork", @"ghost", @"priest"];
	return professions[profession];
}

@end
