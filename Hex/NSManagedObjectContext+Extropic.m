//
//  NSManagedObjectContext+Extropic.m
//  Hex
//
//  Created by Joshua Stewart on 1/11/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

#import "NSManagedObjectContext+Extropic.h"

@implementation NSManagedObjectContext (Extropic)

// Taken from http://www.cocoawithlove.com/2008/03/core-data-one-line-fetch.html/
- (NSSet*)fetchObjectsForEntityName:(NSString*)newEntityName withPredicate:(id)stringOrPredicate, ... {
	NSEntityDescription* entity = [NSEntityDescription entityForName:newEntityName inManagedObjectContext:self];
	NSFetchRequest* request = [[NSFetchRequest alloc] init];
	[request setEntity:entity];

	if (stringOrPredicate) {
		NSPredicate* predicate;
		if ([stringOrPredicate isKindOfClass:[NSString class]]) {
			va_list variadicArguments;
			va_start(variadicArguments, stringOrPredicate);
			predicate = [NSPredicate predicateWithFormat:stringOrPredicate arguments:variadicArguments];
			va_end(variadicArguments);
		} else {
			assert([stringOrPredicate isKindOfClass:[NSPredicate class]]);
			predicate = (NSPredicate*) stringOrPredicate;
		}
		[request setPredicate:predicate];
	}

	NSError* error = nil;
	NSArray* results = [self executeFetchRequest:request error:&error];
	if (error != nil) {
		[NSException raise:NSGenericException format:@"%@", error.description];
	}

	return [NSSet setWithArray:results];
}

@end
