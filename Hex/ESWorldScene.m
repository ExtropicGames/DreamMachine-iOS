
#import <stdlib.h>

#import "ESMapRenderer.h"
#import "ESCombatScene.h"

#import "ESWorldScene.h"

@implementation ESWorldScene
{
	ESAppDelegate* _app;
    ESMapRenderer* _mapRenderer;
	NSIntegerPoint _playerTile;
}

#pragma mark - SKScene

-(id)initWithSize:(CGSize)size {

	self = [super initWithSize:size];
	if (!self) {
		return self;
	}

	self.scaleMode = SKSceneScaleModeResizeFill;
	_app = [UIApplication sharedApplication].delegate;

	// Load a map into the map renderer
	_mapRenderer = [[ESMapRenderer alloc] initWithScene:self];
	_mapRenderer.map = [[ESMap alloc] initWithJSONFile:@"MapDefault"];

	// Initialize the player
	_playerTile = NSIntegerPointMake(10, 10);
	SKSpriteNode* player = [SKSpriteNode spriteNodeWithImageNamed:@"mountain"];
	player.position = CGPointMake(self.size.width / 2,
								  (self.size.height / 2) + (3 * TILE_SIZE) - ((TILE_SIZE - player.size.height) / 2));
	[self addChild:player];
	[_mapRenderer scrollToTile:_playerTile];

    return self;
}

#pragma mark - UIResponder

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];

		if (![_mapRenderer isScrolling]) {

			BOOL moved = false;
			CGPoint offset = CGPointMake(location.x - (self.size.width / 2), location.y - (self.size.height / 2));

			if (offset.x > offset.y && offset.x < -offset.y) {
				_playerTile.y -= 1;
				moved = true;
			} else if (offset.x > offset.y) {
				_playerTile.x += 1;
				moved = true;
			} else if (offset.x < -offset.y) {
				_playerTile.x -= 1;
				moved = true;
			} else {
				_playerTile.y += 1;
				moved = true;
			}

			if (moved) {
				[_mapRenderer scrollToTile:_playerTile];
				int chance = arc4random_uniform(100);
				if (chance < 99) {
					// TODO: this transition sucks. find a better one
					ESCombatScene* combatScene = [[ESCombatScene alloc] initWithSize:self.size];
					[self.scene.view presentScene:combatScene transition:nil];
				}
			}
		}
    }
}

@end
