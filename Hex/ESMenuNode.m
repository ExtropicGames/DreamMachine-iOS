
#import "ESMenuNode.h"

// This is equal to 4x the size in pixels of the part of the menu that is outside the menuFrameBounds. (the corner widgets)
const int MARGIN = 4;
const NSTimeInterval SPEED = 0.3;

@implementation ESMenuNode
{
	SKSpriteNode* cornerTopLeft;
	SKSpriteNode* cornerTopRight;
	SKSpriteNode* cornerBottomLeft;
	SKSpriteNode* cornerBottomRight;
	SKSpriteNode* borderTop;
	SKSpriteNode* borderBottom;
	SKSpriteNode* borderLeft;
	SKSpriteNode* borderRight;
    SKSpriteNode* background;
}

#pragma mark - Initialization

- (instancetype) init {
    self = [super init];
    if (self) {
		[self ESMenuNode_sharedInit];
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
		[self ESMenuNode_sharedInit];
    }
    return self;
}

- (void) ESMenuNode_sharedInit {
	// Defaults
	self.userInteractionEnabled = YES;
	_isOpen = false;
	_textAlign = ESMenuTextAlignLeft;

    _menuBoundsNode = [SKCropNode node];
    _menuBoundsNode.maskNode = [SKSpriteNode spriteNodeWithColor:[UIColor blackColor] size:CGSizeMake(1, 1)];

    self.backgroundImageName = @"background";
    self.borderImageName = @"border";
    self.cornerImageName = @"corner";

    [_menuBoundsNode addChild:background];
    [_menuBoundsNode addChild:borderTop];
    [_menuBoundsNode addChild:borderBottom];
    [_menuBoundsNode addChild:borderLeft];
    [_menuBoundsNode addChild:borderRight];
    [self addChild:_menuBoundsNode];

    NSAssert(CGPointEqualToPoint(_menuBoundsNode.position, CGPointZero), @"Expected the menuBoundsNode to be centered in its parent node.");
    NSAssert(CGRectEqualToRect(_menuBoundsNode.frame, CGRectZero), @"Expected the menuBoundsNode frame to be 0.");
}

#pragma mark - SKNode

- (NSString*) description {
	return [NSString stringWithFormat:@"{%@ at %@ with frame %@}", self.class, NSStringFromCGPoint(self.position), NSStringFromCGRect(self.frame)];
}

- (CGRect) frame {
    return CGRectMake(self.position.x - (self.size.width/2), self.position.y - (self.size.height/2), self.size.width, self.size.height);
}

#pragma mark - ESMenu

- (void) open {
	[self openWithCompletion:nil];
}

- (void) openWithCompletion:(void (^)())completion {

	if (_isOpen || [_menuBoundsNode.maskNode hasActions]) {
		// TODO: This should probably be a little nicer about it, but I'm not sure how to handle the completion block cleanly.
        if (completion) {
            NSAssert(false, @"Called openWithCompletion on an already open %@", self.class);
        }
        return;
    }

	[_menuBoundsNode.maskNode runAction:[SKAction scaleXTo:self.size.width - MARGIN y:self.size.height - MARGIN duration:SPEED] completion:^{
		_isOpen = YES;
		if (completion) {
			completion();
		}
	}];

    [self addChild:cornerTopLeft];
    [self addChild:cornerTopRight];
    [self addChild:cornerBottomLeft];
    [self addChild:cornerBottomRight];

	[cornerTopLeft runAction:[SKAction moveTo:CGPointMake(-(self.size.width / 2) + MARGIN, (self.size.height / 2) - MARGIN) duration:SPEED]];
	[cornerTopRight runAction:[SKAction moveTo:CGPointMake((self.size.width / 2) - MARGIN, (self.size.height / 2) - MARGIN) duration:SPEED]];
	[cornerBottomLeft runAction:[SKAction moveTo:CGPointMake(-(self.size.width / 2) + MARGIN, -(self.size.height / 2) + MARGIN) duration:SPEED]];
	[cornerBottomRight runAction:[SKAction moveTo:CGPointMake((self.size.width / 2) - MARGIN, -(self.size.height / 2) + MARGIN) duration:SPEED]];
	[borderTop runAction:[SKAction moveTo:CGPointMake(0, (self.size.height / 2) - MARGIN) duration:SPEED]];
	[borderBottom runAction:[SKAction moveTo:CGPointMake(0, (-self.size.height / 2) + MARGIN) duration:SPEED]];
	[borderLeft runAction:[SKAction moveTo:CGPointMake((self.size.width / 2) - MARGIN, 0) duration:SPEED]];
	[borderRight runAction:[SKAction moveTo:CGPointMake((-self.size.width / 2) + MARGIN, 0) duration:SPEED]];
}

- (void) close {
	[self closeWithCompletion:nil];
}

- (void) closeWithCompletion:(void (^)())completion {

	if (!_isOpen && !_menuBoundsNode.hasActions) {
		// TODO: This should probably be a little nicer about it, but I'm not sure how to handle the completion block cleanly.
        if (completion) {
            NSAssert(false, @"Called closeWithCompletion on an already closed %@", self.class);
        }

        return;
	}

	[cornerTopLeft runAction:[SKAction moveTo:CGPointMake(0, 0) duration:SPEED]];
	[cornerTopRight runAction:[SKAction moveTo:CGPointMake(0, 0) duration:SPEED]];
	[cornerBottomLeft runAction:[SKAction moveTo:CGPointMake(0, 0) duration:SPEED]];
	[cornerBottomRight runAction:[SKAction moveTo:CGPointMake(0, 0) duration:SPEED]];
	[borderTop runAction:[SKAction moveTo:CGPointMake(0, 0) duration:SPEED]];
	[borderBottom runAction:[SKAction moveTo:CGPointMake(0, 0) duration:SPEED]];
	[borderLeft runAction:[SKAction moveTo:CGPointMake(0, 0) duration:SPEED]];
	[borderRight runAction:[SKAction moveTo:CGPointMake(0, 0) duration:SPEED]];

	[_menuBoundsNode.maskNode runAction:[SKAction scaleXTo:0 y:0 duration:SPEED] completion:^{
		[cornerTopLeft removeFromParent];
        [cornerTopRight removeFromParent];
        [cornerBottomLeft removeFromParent];
        [cornerBottomRight removeFromParent];
		_isOpen = NO;
		if (completion) {
			completion();
		}
	}];
}

// Override on inherited classes
- (void) repositionContents {}

#pragma mark - ESMenu getters & setters

- (void) setSize:(CGSize)size {
    if (CGSizeEqualToSize(_size, size)) {
        return;
    }
    // TODO: only allow setting openedSize on menus that are closed or in the process of closing (not opened or opening)
    _size = size;
    [self repositionContents];
}

- (void) setTextAlign:(ESMenuTextAlign)textAlign {
    if (textAlign == _textAlign) {
        return;
    }
    _textAlign = textAlign;
    [self repositionContents];
}

- (void) setBackgroundImageName:(NSString *)backgroundImageName {
    _backgroundImageName = backgroundImageName;

    if (!background) {
        background = [[SKSpriteNode alloc] init];
    }

    background.texture = [SKTexture textureWithImageNamed:_backgroundImageName];
    background.size = background.texture.size;
}

- (void) setBorderImageName:(NSString *)borderImageName {
    _borderImageName = borderImageName;

    if (!borderTop) {
        borderTop = [[SKSpriteNode alloc] init];
        borderBottom = [[SKSpriteNode alloc] init];
        borderLeft = [[SKSpriteNode alloc] init];
        borderRight = [[SKSpriteNode alloc] init];
        borderLeft.zRotation = M_PI_2;
        borderRight.zRotation = M_PI_2;
    }

    borderTop.texture = [SKTexture textureWithImageNamed:_borderImageName];
    borderBottom.texture = [SKTexture textureWithImageNamed:_borderImageName];
    borderLeft.texture = [SKTexture textureWithImageNamed:_borderImageName];
    borderRight.texture = [SKTexture textureWithImageNamed:_borderImageName];
    borderTop.size = borderTop.texture.size;
    borderBottom.size = borderBottom.texture.size;
    borderLeft.size = borderLeft.texture.size;
    borderRight.size = borderRight.texture.size;
}

- (void) setCornerImageName:(NSString *)cornerImageName {

    _cornerImageName = cornerImageName;

    if (!cornerTopLeft) {
        cornerTopLeft = [[SKSpriteNode alloc] init];
        cornerTopRight = [[SKSpriteNode alloc] init];
        cornerBottomLeft = [[SKSpriteNode alloc] init];
        cornerBottomRight = [[SKSpriteNode alloc] init];
        cornerTopRight.zRotation = -M_PI_2;
        cornerBottomLeft.zRotation = M_PI_2;
        cornerBottomRight.zRotation = M_PI;
    }

    cornerTopLeft.texture = [SKTexture textureWithImageNamed:_cornerImageName];
    cornerTopRight.texture = [SKTexture textureWithImageNamed:_cornerImageName];
    cornerBottomLeft.texture = [SKTexture textureWithImageNamed:_cornerImageName];
    cornerBottomRight.texture = [SKTexture textureWithImageNamed:_cornerImageName];
    cornerTopLeft.size = cornerTopLeft.texture.size;
    cornerTopRight.size = cornerTopRight.texture.size;
    cornerBottomLeft.size = cornerBottomLeft.texture.size;
    cornerBottomRight.size = cornerBottomRight.texture.size;
}

@end
