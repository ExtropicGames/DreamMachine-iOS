
import UIKit

@objc
protocol NameMenuNodeDelegate {
    func nameMenuDidCompleteWithName(String)
}

class NameMenuNode: ESMenuNode {

    var nameLabel: SKLabelNode
    var characterLabels: SKLabelNode[]
    var delegate: NameMenuNodeDelegate?

    init() {
        delegate = nil
        nameLabel = SKLabelNode(fontNamed:"Terminus")
        nameLabel.fontSize = 16 // TODO: this is the FONT_SIZE constant but not sure how to do that in Swift
        nameLabel.horizontalAlignmentMode = .Left
        nameLabel.verticalAlignmentMode = .Baseline
        nameLabel.text = "_ _ _ _ _ _ _ _"

        let characterSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        characterLabels = []
        for c in characterSet {
            let labelNode = SKLabelNode()
            labelNode.text = "\(c)"
            labelNode.verticalAlignmentMode = .Baseline
            labelNode.horizontalAlignmentMode = .Center
            characterLabels.append(labelNode)
        }

        super.init()

        self.menuBoundsNode.addChild(nameLabel)
        for cl in characterLabels {
            self.menuBoundsNode.addChild(cl)
        }
    }

    override func repositionContents() {

        nameLabel.position = CGPointMake(-self.size.width/2, (self.size.height/2) - nameLabel.frame.size.height);

        let keyboardRegion = CGRectMake(0, nameLabel.frame.size.height, self.size.width, self.size.height - nameLabel.frame.size.height);

        let colCount: Float = 10.0;
        let rowCount: Float = Float(characterLabels.count) / colCount;

        let colWidth: Float = keyboardRegion.size.width / colCount;
        let rowHeight: Float = keyboardRegion.size.height / rowCount;

        var row: Float = 0;
        var col: Float = 0;

        for cLabel in characterLabels {
            var x: Float = keyboardRegion.origin.x + (-keyboardRegion.size.width/2) + ((col + 0.5) * colWidth);
            var y: Float = keyboardRegion.origin.y + (keyboardRegion.size.height/2) - ((row + 0.5) * rowHeight);
            cLabel.position = CGPointMake(x, y);

            col++;
            if (col >= colCount) {
                col = 0;
                row++;
            }
        }
    }
}

class NameMenuHeaderNode: ESMenuNode {

}

class NameMenuBodyNode: ESChoiceMenuNode {

}